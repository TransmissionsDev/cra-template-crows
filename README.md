# Crows &middot; ![Crows](https://img.shields.io/badge/Made%20with-Crows-black?logo=react&logoColor=white)

Transmissions's template for quick and easy React apps.

## Replace these template strings

Use your editor's find all in path tool to replace these template strings.
_All template strings to be replaced are in `SCREAMING_SNAKE_CASE`_

- **YOUR_APP:** The short, human readable name of your app.

  - Examples: "Discord", "PhotoShop Mix"

- **SHORT_DESCRIPTION:** A short description of your app.

  - Example: "Tinder for dogs", "Chat for the 21st century"
